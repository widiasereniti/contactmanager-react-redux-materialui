import axios from 'axios'
import { API_URL, API_TIMEOUT } from '../config/constant'

export const GET_CONTACT = "GET_CONTACT"
export const GET_CONTACT_DETAIL = "GET_CONTACT_DETAIL"
export const POST_ADD_CONTACT = "POST_ADD_CONTACT"
export const POST_EDIT_CONTACT = "POST_EDIT_CONTACT"
export const DELETE_CONTACT = "DELETE_CONTACT"

export const getContact = () => {
  return (dispatch) => {
    dispatch({
      type: GET_CONTACT,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });

    axios({
      method: "get",
      url: API_URL + `contact`,
      timeout: API_TIMEOUT,
    })
    .then(response => {
      // console.log(response.data.data)
        if (!response.data.data) {
          dispatch({
            type: GET_CONTACT,
            payload: {
                loading: false,
                data: false,
                errorMessage: false,
            }
          })
          
        } else {
          // console.log(response.data)
          dispatch({
            type: GET_CONTACT,
            payload: {
                loading: false,
                data: response.data.data,
                errorMessage: false,
            }
          })
        }
    })
    .catch(error => {
        dispatch({
            type: GET_CONTACT,
            payload: {
              loading: false,
              data: false,
              errorMessage: error
            }
        })
    })
  }
}

export const getContactDetail = (id) => {
  return (dispatch) => {
    dispatch({
      type: GET_CONTACT_DETAIL,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });

    axios({
      method: "get",
      url: API_URL + `contact/${id}`,
      timeout: API_TIMEOUT,
    })

    .then(response => {
      // console.log(response.data.data)
        if (!response.data.data) {
          dispatch({
              type: GET_CONTACT_DETAIL,
              payload: {
                  loading: false,
                  data: false,
                  errorMessage: false,
              }
          })
        } else {
          dispatch({
            type: GET_CONTACT_DETAIL,
            payload: {
                loading: false,
                data: response.data.data,
                errorMessage: false,
            }
          }) 
        }
    })
    .catch(error => {
        dispatch({
            type: GET_CONTACT_DETAIL,
            payload: {
              loading: false,
              data: false,
              errorMessage: error
            }
        })
    })
  }
}

export const postAddContact = (firstName, lastName, age, photo) => {
    const data = {
        firstName: firstName,
        lastName: lastName,
        age: age,
        photo: photo
    }
    console.log(data)

    return (dispatch) => {
      dispatch({
        type: POST_ADD_CONTACT,
        payload: {
          loading: true,
          data: false,
        },
      });
  
      axios({
        method: "post",
        url: API_URL + "contact",
        timeout: API_TIMEOUT,
        data: data
      })
      .then(response => {
        // console.log(response.data)
          if (!response.data) {
            dispatch({
              type: POST_ADD_CONTACT,
              payload: {
                  loading: false,
                  data: false,
                  errorMessage: false,
              }
            })
          } else {
            dispatch({
              type: POST_ADD_CONTACT,
              payload: {
                  loading: false,
                  data: response.data,
                  errorMessage: false,
              }
            })
          }
      })
      .catch(error => {
          dispatch({
              type: POST_ADD_CONTACT,
              payload: {
              loading: false,
              data: false,
              errorMessage: error
              }
          })
      })
    }
}

export const postEditContact = (id, firstName, lastName, age, photo ) => {
  const data = {
      firstName: firstName,
      lastName: lastName,
      age: age,
      photo: photo
  }
  console.log(data)

  return (dispatch) => {
    dispatch({
      type: POST_EDIT_CONTACT,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });

    axios({
      method: "put",
      url: API_URL + `contact/${id}`,
      timeout: API_TIMEOUT,
      data: data
    })

    .then(response => {
      console.log(response.data)
        if (!response.data) {
          dispatch({
            type: POST_EDIT_CONTACT,
            payload: {
                loading: false,
                data: false,
                errorMessage: false,
            }
          })
        } else {
          dispatch({
              type: POST_EDIT_CONTACT,
              payload: {
                  loading: false,
                  data: response.data.data,
                  errorMessage: false,
              }
          })
        }
    })
    .catch(error => {
        dispatch({
            type: POST_EDIT_CONTACT,
            payload: {
              loading: false,
              data: false,
              errorMessage: error
            }
        })
    })
  }
}

export const deleteContact = (id) => {
  console.log(id)
  return (dispatch) => {
    dispatch({
      type: DELETE_CONTACT,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });

    axios({
      method: "delete",
      url: API_URL + `contact/${id}`,
      timeout: API_TIMEOUT,
    })

    .then(response => {
      console.log(response)
        if (!response.data.data) {
          dispatch({
              type: DELETE_CONTACT,
              payload: {
                  loading: false,
                  data: false,
                  errorMessage: false,
              }
          })
        } else {
          dispatch({
            type: DELETE_CONTACT,
            payload: {
                loading: false,
                data: response.data.data,
                errorMessage: false,
            }
          }) 
        }
    })
    .catch(error => {
        dispatch({
            type: DELETE_CONTACT,
            payload: {
              loading: false,
              data: false,
              errorMessage: error
            }
        })
    })
  }
}

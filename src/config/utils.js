import { NOTE_IMAGE_SIZE } from './constant'

export const regexSymbolNum = (newValue) => {
    var output = /[*+=_?^${/}<>&%(),.:;'^~!@#`\\\\|"[\]\\\n\\\t\\\f\\\r\\\n\\\-\\0-9]/g.test(newValue)
    return output
}

export const regexNumOnly = (newValue) => {
    var output = /[A-Za-z *+=_.,?^${/}<>&%():;'^~!@#`\\\\|"[\]\\\n\\\t\\\f\\\r\\\n\\\-\\]/g.test(newValue)
    return output
}

export const imageTypeValidate = (newValue) => {
    if (newValue !== "image/png" && newValue !== "image/jpg" && newValue !== "image/jpeg") {
      return false
    } else {
      return true
    }
}

export const imageSizeValidate = (newValue) => {
    if (newValue > NOTE_IMAGE_SIZE) {
        return false
    } else {
        return true
    }
}
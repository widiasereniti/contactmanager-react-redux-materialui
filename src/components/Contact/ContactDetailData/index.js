import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Typography } from '@material-ui/core';
import { deleteContact } from '../../../actions/ContactAction'
import swal from 'sweetalert'

const mapStateToProps = (state) => ({
  getContactDetailData: state.ContactReducer.getContactDetailData,
})

class ContactDetailData extends Component {
  constructor(props){
    super(props)

    this.handleDeleteContact = this.handleDeleteContact.bind(this)
  }

  handleDeleteContact(event){
    const id = event.currentTarget.value;

    swal({
        title: "Delete Contact",
        text: "Are you sure delete this contact?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((value) => {
        if (value){
            const { deleteContact} = this.props
            deleteContact(id)
        }
    });
  }


  render() {
    const {getContactDetailData} = this.props
    return (
      <div>
        <Button variant="contained" color="secondary" href={`/`}>Back</Button>
        <div className="container-img">
        {
            getContactDetailData ?
                <React.Fragment>
                    <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                        {getContactDetailData.firstName}{' '}{getContactDetailData.lastName}
                    </Typography>
                    <Typography component="img" style={{height: 300, width: 'auto'}} src={getContactDetailData.photo} />
                    <Typography component="h2" style={{textAlign:'center', margin: '5px'}}>
                        <strong>First Name: </strong>{getContactDetailData.firstName}<br/> 
                        <strong>Last Name: </strong>{getContactDetailData.lastName}<br/> 
                        <strong>Age: </strong>{getContactDetailData.age}<br/>                              
                    </Typography>
                    <Typography style={{marginBottom: '2em'}}>
                        <Button href={`/edit/` + getContactDetailData.id} className="float-left" style={{backgroundColor: '#e3ba24', color: 'white', borderColor: '#e3ba24'}}> Edit</Button>
                        <Button onClick={this.handleDeleteContact} value={getContactDetailData.id} className="float-right" style={{backgroundColor: 'red', color: 'white', borderColor: '#e3ba24'}}>Delete</Button>
                    </Typography>
                </React.Fragment>
            : 
            <Typography variant="h5" component="h2" style={{textAlign:'center', fontWeight: 'bold', margin: '5px'}}>
                Data Not Found
            </Typography>
        }
        </div>
    </div>
    )
  }
}


export default (connect(mapStateToProps, { deleteContact })(ContactDetailData))
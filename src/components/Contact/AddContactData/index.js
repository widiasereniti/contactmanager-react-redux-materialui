import React, { Component } from 'react'
import { reduxForm, Field, change } from 'redux-form'
import { Card, CardBody, CardHeader, Label, Col, Row } from 'reactstrap'
import { connect } from 'react-redux';
import { AddContactVal } from '../../../config/validate/AddContactVal';
import { Button, TextField, Grid, Typography } from '@material-ui/core'
import { bindActionCreators } from 'redux';

const renderField = ({
    input,
    type,
    placeholder,
    label,
    disabled,
    readOnly,
    optional,
    meta: { touched, error, warning }
}) => (
    <Row>
        <Grid md="3"><Label htmlFor="{input}" className="col-form-label">{label}</Label><span><i> {optional}</i></span></Grid>
        <Grid md="7"><TextField {...input} type={type} placeholder={placeholder} disabled={disabled} 
        readOnly={
            readOnly}>
        </TextField>
        {touched &&
            ((error && <p style={{ color: 'red' }}>{error}</p>) || (warning && <p style={{ color: 'brown' }}>{warning}</p>))
        }</Grid>
    </Row>
)

const mapStateToProps = (state) => ({
    initialValues: {
        firstName: '',
        lastName: '',
        age: '',
        photo: '',
    },
})

class AddContactData extends Component {
    render() {
        const { handleSubmit} = this.props


        return (
            <Card>
                <form onSubmit={handleSubmit}>
                    <CardHeader style={{lineHeight: "2.3"}}>
                    <Button className="float-right" variant="contained" color="secondary" href={`/`} styles={{margin: '20px'}}>
                        Back
                    </Button>
                    </CardHeader>
                    <CardBody>
                        <h1 style={{textAlign: 'center'}}>ADD CONTACT</h1>
                        <Grid row className="my-0">
                            <Col md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="firstName"
                                    component={renderField}
                                    label="First Name" 
                                    /> 
                                </Grid>
                            </Col>
                            <Col md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="lastName"
                                    component={renderField}
                                    label="Last Name" /> 
                                </Grid>
                            </Col>
                            <Col md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="age"
                                    component={renderField} 
                                    label="Age" /> 
                                </Grid>
                            </Col>
                            <Col md="12">
                                <Grid > 
                                    <Field 
                                    type="text"
                                    name="photo"
                                    component={renderField}
                                    label="Image Url" /> 
                                </Grid>
                            </Col>
                        </Grid>
                    </CardBody>
                    <CardBody>
                        <Grid row className="my-0">
                            <Col md="12">
                                <Grid>
                                    <Button 
                                        color="danger" 
                                        type="submit" 
                                        className="float-right">Submit
                                    </Button>
                                </Grid>
                            </Col>
                        </Grid>
                    </CardBody>
                </form>
            </Card>       
        )
  }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
    }, dispatch);
}

AddContactData = reduxForm({
    form: 'formAddContact',
    validate: AddContactVal,
    enableReinitialize: true
})(AddContactData)

export default connect(mapStateToProps, mapDispatchToProps)(AddContactData)
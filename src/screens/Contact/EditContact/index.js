import React, { Component } from 'react';
import { connect } from 'react-redux';
import EditContactData from '../../../components/Contact/EditContactData/index'
import { getContactDetail, postEditContact } from '../../../actions/ContactAction'
import swal from 'sweetalert'

const mapStateToProps = (state) => ({
    postEditContactDataLoading: state.ContactReducer.postEditContactDataLoading,
    postEditContactData: state.ContactReducer.postEditContactData,
    postEditContactDataError: state.ContactReducer.postEditContactDataError,
})

class EditContact extends Component {
  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount(){
    const { getContactDetail } = this.props
    getContactDetail(this.props.match.params.id)
  }

  handleSubmit(data){        
    const { postEditContact } = this.props
      swal({
          title: "Submit Data Awal",
          text: "Apakah anda yakin ingin submit data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((value) => {
          if (value) {
            postEditContact(
              data.id,
              data.firstName,
              data.lastName,
              data.age,
              data.photo
            )
          }
      }); 
  }

  componentDidUpdate(prevProps, prevState){
    const {postEditContactData} = this.props
    if (postEditContactData !== "" && prevProps.postEditContactData !== postEditContactData) {
      window.location.replace('/')
    }
  }

  render() {
    return (
      <div>
        <EditContactData onSubmit={this.handleSubmit}/>
      </div>
    )
  }
}

export default (connect(mapStateToProps, { getContactDetail, postEditContact })(EditContact))
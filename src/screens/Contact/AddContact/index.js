import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddContactData from '../../../components/Contact/AddContactData/index'
import { postAddContact } from '../../../actions/ContactAction'
import swal from 'sweetalert'

const mapStateToProps = (state) => ({
    postAddContactDataLoading: state.ContactReducer.postAddContactDataLoading,
    postAddContactData: state.ContactReducer.postAddContactData,
    postAddContactDataError: state.ContactReducer.postAddContactDataError,
})

class AddContact extends Component {
  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(data){        
    const { postAddContact } = this.props
      swal({
          title: "Submit Contact",
          text: "Are you sure save this contact?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((value) => {
          if (value) {
            postAddContact(
              data.firstName,
              data.lastName,
              data.age,
              data.photo
            )
          }
      }); 
  }

  componentDidUpdate(prevProps, prevState){
    const {postAddContactData} = this.props
    if (postAddContactData !== "" && prevProps.postAddContactData !== postAddContactData) {
      window.location.replace('/')
    }
  }

  render() {
    return (
      <div>
        <AddContactData onSubmit={this.handleSubmit}/>
      </div>
    )
  }
}

export default (connect(mapStateToProps, { postAddContact })(AddContact))
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ContactListData from '../../../components/Contact/ContactListData/index'
import { getContact } from '../../../actions/ContactAction'
const mapStateToProps = (state) => ({
//   getSuccessDataLoading: state.TransactionReducer.getSuccessDataLoading,
//   getSuccessData: state.TransactionReducer.getSuccessData,
//   getSuccessDataError: state.TransactionReducer.getSuccessDataError,
//   successData: window.localStorage.getItem("success")
})

class ContactList extends Component {
  componentDidMount(){
    const { getContact } = this.props
    getContact()
  }

  render() {
    return (
      <div>
        <ContactListData />
      </div>
    )
  }
}

export default (connect(mapStateToProps, { getContact })(ContactList))

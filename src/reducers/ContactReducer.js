import { 
    GET_CONTACT, GET_CONTACT_DETAIL, POST_ADD_CONTACT, POST_EDIT_CONTACT, DELETE_CONTACT
} from '../actions/ContactAction'
  
  const initialState = {
    getContactDataLoading: false,
    getContactData: false,
    getContactDataError: false,

    getContactDetailDataLoading: false,
    getContactDetailData: false,
    getContactDetailDataError: false,

    postAddContactDataLoading: false,
    postAddContactData: false,
    postAddContactDataError: false,

    postEditContactDataLoading: false,
    postEditContactData: false,
    postEditContactDataError: false,

    deleteContactDataLoading: false,
    deleteContactData: false,
    deleteContactDataError: false,
  }
  
  export default function (state = initialState, action) {
    switch (action.type) {
  
        case GET_CONTACT:
            return {
                ...state,
                getContactDataLoading: action.payload.loading,
                getContactData: action.payload.data,
                getContactDataError: action.payload.errorMessage,
            }
        case GET_CONTACT_DETAIL:
            return {
                ...state,
                getContactDetailDataLoading: action.payload.loading,
                getContactDetailData: action.payload.data,
                getContactDetailDataError: action.payload.errorMessage,
            }
        case POST_ADD_CONTACT:
            return {
                ...state,
                postAddContactDataLoading: action.payload.loading,
                postAddContactData: action.payload.data,
                postAddContactDataError: action.payload.errorMessage,
            }
        case POST_EDIT_CONTACT:
            return {
                ...state,
                postEditContactDataLoading: action.payload.loading,
                postEditContactData: action.payload.data,
                postEditContactDataError: action.payload.errorMessage,
            }
        case DELETE_CONTACT:
            return {
                ...state,
                deleteContactDataLoading: action.payload.loading,
                deleteContactData: action.payload.data,
                deleteContactDataError: action.payload.errorMessage,
            }
        default:
            return state
    }
  }
  